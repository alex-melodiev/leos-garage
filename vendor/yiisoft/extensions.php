<?php

$vendorDir = dirname(__DIR__);

return array (
  'asofter/yii2-imperavi-redactor' => 
  array (
    'name' => 'asofter/yii2-imperavi-redactor',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii/imperavi' => $vendorDir . '/asofter/yii2-imperavi-redactor/yii/imperavi',
    ),
  ),
  'trntv/yii2-aceeditor' => 
  array (
    'name' => 'trntv/yii2-aceeditor',
    'version' => '2.0.0.0',
    'alias' => 
    array (
      '@trntv/aceeditor' => $vendorDir . '/trntv/yii2-aceeditor',
    ),
  ),
  'yiisoft/yii2-jui' => 
  array (
    'name' => 'yiisoft/yii2-jui',
    'version' => '2.0.3.0',
    'alias' => 
    array (
      '@yii/jui' => $vendorDir . '/yiisoft/yii2-jui',
    ),
  ),
  'trntv/yii2-file-kit' => 
  array (
    'name' => 'trntv/yii2-file-kit',
    'version' => '1.0.0.0',
    'alias' => 
    array (
      '@trntv/filekit' => $vendorDir . '/trntv/yii2-file-kit/src',
    ),
  ),
  'trntv/yii2-bootstrap-datetimepicker' => 
  array (
    'name' => 'trntv/yii2-bootstrap-datetimepicker',
    'version' => '1.0.0.0',
    'alias' => 
    array (
      '@trntv/yii/datetimepicker' => $vendorDir . '/trntv/yii2-bootstrap-datetimepicker',
    ),
  ),
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '2.0.3.0',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer',
    ),
  ),
  'yiisoft/yii2-authclient' => 
  array (
    'name' => 'yiisoft/yii2-authclient',
    'version' => '2.0.3.0',
    'alias' => 
    array (
      '@yii/authclient' => $vendorDir . '/yiisoft/yii2-authclient',
    ),
  ),
  'yiisoft/yii2-imagine' => 
  array (
    'name' => 'yiisoft/yii2-imagine',
    'version' => '2.0.3.0',
    'alias' => 
    array (
      '@yii/imagine' => $vendorDir . '/yiisoft/yii2-imagine',
    ),
  ),
  'mihaildev/yii2-elfinder' => 
  array (
    'name' => 'mihaildev/yii2-elfinder',
    'version' => '1.1.2.0',
    'alias' => 
    array (
      '@mihaildev/elfinder' => $vendorDir . '/mihaildev/yii2-elfinder',
    ),
  ),
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '2.0.3.0',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '2.0.3.0',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.0.3.0',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii',
    ),
  ),
  'yiisoft/yii2-codeception' => 
  array (
    'name' => 'yiisoft/yii2-codeception',
    'version' => '2.0.3.0',
    'alias' => 
    array (
      '@yii/codeception' => $vendorDir . '/yiisoft/yii2-codeception',
    ),
  ),
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '2.0.3.0',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker',
    ),
  ),
  'sammaye/yii2-mailchimp' => 
  array (
    'name' => 'sammaye/yii2-mailchimp',
    'version' => '0.1.1.0',
    'alias' => 
    array (
      '@sammaye/mailchimp' => $vendorDir . '/sammaye/yii2-mailchimp',
    ),
  ),
  'rmrevin/yii2-minify-view' => 
  array (
    'name' => 'rmrevin/yii2-minify-view',
    'version' => '1.8.6.0',
    'alias' => 
    array (
      '@rmrevin/yii/minify' => $vendorDir . '/rmrevin/yii2-minify-view',
    ),
  ),
);
