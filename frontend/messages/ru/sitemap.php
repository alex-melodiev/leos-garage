<?php
/**
 * Created by PhpStorm.
 * User: dusty
 * Date: 28.12.15
 * Time: 11:25
 */

return [

    'Story' => 'О нас',
    'Our story' => 'Наша история',
    'Here’s my pit crew.' => 'Вот моя команда',
    'In our world, a car mechanic is like a doctor – he needs to have the knowledge to make a diagnosis and have impeccable bedside manner. And we’d like to think we have that, but don’t take our word for it. From the Ford Model-T to the latest iteration of the Bull, the DNA of cars hasn\'t changed since their creation. At Leo’s, we consider ourselves not just Dubai’s new car experts, but true car enthusiasts so there’s very (very very) little we can’t cure . Some of us may sound, say, exotic but we know how to speak with cars in any language.' => 'В нашем представлении, автомеханик - это доктор, которому требуются как знания о диагностике проблемы, так и особая манера их решения. И с удовольствием отмечаем, что у нас есть и то, и другое - но не верьте нам на слово. Начиная с Ford Model-T, и заканчиваю последней версией \"Быка\" генетика автомобилей не изменилась. В Leo\'s, мы считаем себя не просто экспертами по автомобилям в масштабах Дубаи, но настоящими автоэнтузиастами, которым очень (очень-очень) мало что будет не по плечу. Некоторые из нас имеют экзотический акцент, но каждый из нас свободно общается с автомобилем из любой страны',

];