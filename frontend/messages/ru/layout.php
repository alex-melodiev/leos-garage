<?php
/**
 * Created by PhpStorm.
 * User: dusty
 * Date: 27.12.15
 * Time: 14:54
 */

return [

    // Bottom menu
    'COMPANY' => 'Компания',
    'About Us' => 'О нас',
    'News' => 'Новости',
    'Garage Location' => 'Местоположение гаража',
    'SERVICE' => 'Сервис',
    'Services' => 'Услуги',
    'FAQ' => 'FAQ',
    'Contact Us' => 'Обратная связь',
    'LEGAL' => 'Правовая информация',
    'Privacy' => 'Защита персональных данных',
    'Terms &amp; Conditions' => 'Положения &amp; условия',

    // footer
    'RIDE TO THE TOP' => 'Наверх',
    'Sitemap' => 'Карта сайта',
    'Toll Free Number' => 'Бесплатный номер',
    'Toll Free' => 'Телефон',
    // right menu slider

    'Our Services' => 'Наши услуги',
    'Our Location' => 'Наше расположение',
    'Our offers' => 'Наши акции',

    // slogan
    'At Leo’s, our vision is to be the garage of choice for car owners who know their cars.' => 'Мы думаем, что мы и есть тот самый автосервис, который выбирают автовладельцы, имеющие представление о своих автомобилях, и понимающие, что машина - не роскошь, а средство передвижения. ',

    'Our location' => 'Как нас найти',
    'Leo\'s Services' => 'Сервисы Leo\'s',
    'leos-en' => 'leos-ru',
    "Leo's" => "Гараж Leo's",
    "Leo's Garage Dubai" => "Leo's Автосервис в Дубай",
];
