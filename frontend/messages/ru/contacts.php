<?php
/**
 * Created by PhpStorm.
 * User: dusty
 * Date: 28.12.15
 * Time: 10:58
 */

return [

    //title
    'Contacts' => 'Обратная связь',
    'Can’t I just text' => 'Почему бы просто не написать?',
    'We’d love to hear from your car but since we’re still teaching them to speak, drop us a line on their behalf.' => 'Мы бы с радостью поговорили напрямую с вашими машинами, но, так как мы все еще учим их говорить, напишите нам пару строк от их имени',
    'Contact us directly' => 'Или напишите нам напрямую'
];