$(document).ready(function() {



// owlcarousel



        function center(number){
            var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
            var num = number;
            var found = false;
            for(var i in sync2visible){
                if(num === sync2visible[i]){
                    var found = true;
                }
            }

            if(found===false){
                if(num>sync2visible[sync2visible.length-1]){
                    sync2.trigger("owl.goTo", num - sync2visible.length+2)
                }else{
                    if(num - 1 === -1){
                        num = 0;
                    }
                    sync2.trigger("owl.goTo", num);
                }
            } else if(num === sync2visible[sync2visible.length-1]){
                sync2.trigger("owl.goTo", sync2visible[1])
            } else if(num === sync2visible[0]){
                sync2.trigger("owl.goTo", num-1)
            }

        }




    $(".flipster").flipster({ style: 'coverflow', enableMousewheel: false, enableNavButtons: true, enableTouch:false });
	$('.tcon').click(function(){
		$(this).toggleClass('tcon-transform');
	});


	
	//ScrollTo
	headerH = $('#header').outerHeight();
		$(function($) {
			$('a.to-top-button').click(function() {

				$.scrollTo(this.hash, 2000, {
					duration : 500,
					offset : {
						top : -headerH
					}
				});

				return false;
			});
		});
		 $(".header-nav").sticky({
            topSpacing: 0,
            height: 0 // делаем отступ от верхнего края страницы
        });
		 
	});

		

/* Search trigger */
function search() {
	var flag = true;
	$('.search-control').click(function(e){
		e.preventDefault();	
		$(this).toggleClass('sclicked');
		$('.header-search').toggleClass('visible');
		if(flag){
			$(this).find('i').removeClass('fa-search');
			$(this).find('i').addClass('fa-close');
			$(this).css('margin-left', 0);
			$('.header-search form .form-control').focus();
			flag = false;
		} else {
			$(this).find('i').removeClass('fa-close');
			$(this).find('i').addClass('fa-search');
			$(this).css('margin-left', 30);
			flag = true;
		}
	});
};
/* Search trigger */
 $(document).ready(function(){
        $("[data-toggle]").click(function() {
          var toggle_el = $(this).data("toggle");
          $(toggle_el).toggleClass("open-sidebar");
        });
         $(".swipe-area").swipe({
              swipeStatus:function(event, phase, direction, distance, duration, fingers)
                  {
                      if (phase=="move" && direction =="left") {
                           $("#wrapper").addClass("open-sidebar");
                           return false;
                      }
                      if (phase=="move" && direction =="right") {
                           $("#wrapper").removeClass("open-sidebar");
                           return false;
                      }
                  }
          }); 
      });