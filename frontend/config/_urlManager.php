<?php
return [
	'class'           => 'common\components\LangUrlManager',
	'enablePrettyUrl' => true,
	'showScriptName'  => false,
	'rules'           => [
		['pattern' => 'ru/sitemap', 'route' => 'site/sitemap'],
		['pattern' => 'ru/site/about', 'route' => 'site/about'],
		['pattern' => 'ru/site/contact', 'route' => 'site/contact'],

		['pattern' => 'ru/service/<slug>', 'route' => 'page/view'],
		['pattern' => 'service/<slug>', 'route' => 'page/view'],

		['pattern' => 'news', 'route' => 'news/index'],
		['pattern' => 'ru/news', 'route' => 'news/index'],
		['pattern' => 'news/cat/<cat>', 'route' => 'news/index'],
		['pattern' => 'ru/news/cat/<cat>', 'route' => 'news/index'],
		['pattern' => 'news/<slug>', 'route' => 'news/view'],
		['pattern' => 'ru/news/<slug>', 'route' => 'news/view'],
		['pattern' => '', 'route' => 'site/index'],
		['pattern' => 'ru', 'route' => 'site/rus'],
		['pattern' => 'sitemap', 'route' => 'site/sitemap'],
	],
];
