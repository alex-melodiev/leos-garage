<?php
$config = [
    'homeUrl'=>Yii::getAlias('@frontendUrl'),
    'controllerNamespace' => 'frontend\controllers',
    'defaultRoute' => 'site/index',
    'modules' => [
        'user' => [
            'class' => 'frontend\modules\user\Module'
        ]
    ],
    'components' => [
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'github' => [
                    'class' => 'yii\authclient\clients\GitHub',
                    'clientId' => getenv('GITHUB_CLIENT_ID'),
                    'clientSecret' => getenv('GITHUB_CLIENT_SECRET')
                ]
            ]
        ],
        'errorHandler' => [
            'errorAction' => 'site/error'
        ],
        'request' => [
            'cookieValidationKey' => getenv('FRONTEND_COOKIE_VALIDATION_KEY')
        ],
        'user' => [
            'class'=>'yii\web\User',
            'identityClass' => 'common\models\User',
            'loginUrl'=>['/user/sign-in/login'],
            'enableAutoLogin' => true
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@frontend/messages',
                    'sourceLanguage' => 'en',
                    'fileMap' => [
                        'common' => 'common.php',
                        'layout' => 'layout.php',
                    ],
                ],
            ],
        ]
    ],

];

if (YII_ENV_DEV) {
    $config['modules']['gii'] = [
        'class'=>'yii\gii\Module',
        'generators'=>[
            'crud'=>[
                'class'=>'yii\gii\generators\crud\Generator',
                'messageCategory'=>'frontend'
            ]
        ]
    ];
	//$config['components']['assetManager']['bundles'] = require(__DIR__ . '/assets-prod.php');
}

if (YII_ENV_PROD) {
    // Maintenance mode
    $config['bootstrap'] = ['maintenance'];
    $config['components']['maintenance'] = [
        'class' => 'common\components\maintenance\Maintenance',
        'enabled' => function ($app) {
            return $app->keyStorage->get('frontend.maintenance') === 'true';
        }
    ];
	$config['components']['view'] = [
		'class' => '\rmrevin\yii\minify\View',
		'enableMinify' => !YII_DEBUG,
		'web_path' => '@web', // path alias to web base
		'base_path' => '@webroot', // path alias to web base
		'minify_path' => '@webroot/assets/minify', // path alias to save minify result
		'js_position' => [ \yii\web\View::POS_END ], // positions of js files to be minified
		'force_charset' => 'UTF-8', // charset forcibly assign, otherwise will use all of the files found charset
		'expand_imports' => true, // whether to change @import on content
		//'compress_output' => true, // compress result html page
	];
	//$config['components']['assetManager']['bundles'] = require(__DIR__ . '/assets-prod.php');
}

return $config;
