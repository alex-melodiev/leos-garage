<?php
namespace frontend\controllers;

use Yii;
use frontend\models\ContactForm;
use yii\web\Controller;
use frontend\models\Lang;

/**
 * Site controller
 */
class SiteController extends Controller
{

    public function __construct($id,$module,$config = [])
    {
        parent::__construct($id,$module,$config);
        Lang::setCurrent(Yii::$app->request->url);

        \Yii::$app->language = Lang::getCurrent()['local'];



    }
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction'
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null
            ],
            'set-locale'=>[
                'class'=>'common\components\action\SetLocaleAction',
                'locales'=>array_keys(Yii::$app->params['availableLocales'])
            ]
        ];
    }

    public function actionIndex()
    {

        return $this->render('indexen');
    }
     public function actionRus()
    {
        //$this->layout = 'rus_base';

        return $this->render('indexru');
    }

    public function actionContact()
    {
		$this->layout = '@frontend/views/layouts/contacts/main';
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post())) {

            // Для отправки HTML-письма должен быть установлен заголовок Content-type
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            $headers .= 'From: [Leo`s Garage] Contact Form  <noreply@leosgarage.ae>' . "\r\n";
            // $headers .= 'Cc: birthdayarchive@example.com' . "\r\n";
//            $headers .= 'Bcc: birthdaycheck@example.com' . "\r\n";
//
//            $headers .= 'Leo`s Garage contact form  <fixit@leosgarage.ae>' . "\r\n";

            $mes = "Dear Leo's Garage management team,<br/><br/>
                You have received a quote request <br>
<br>
                Submitted on ".date('Y.m.d')."<br>
                Submitted values are:<br>
                <br>
                Name: $model->name<br>
           
                Email Address: $model->email<br>
                Phone: ".$model->phone."<br>
                Subject: $model->subject
                Message: $model->body";

            $b = mail(Yii::$app->params['adminEmail'], "Leo's Garage mailing system",$mes, $headers);
            $b = mail('alex.melodiev@gmail.com', "Leo's Garage mailing system", $mes, $headers);
            $b = mail('info@tessellastudio.com', "Leo's Garage mailing system", $mes, $headers);
            $b = mail('fixit@leosgarage.ae', "Leo's Garage mailing system", $mes, $headers);

            //if ($model->contact(Yii::$app->params['adminEmail'])) {
            if($b){
                Yii::$app->getSession()->setFlash('alert', [
                    'body'=>Yii::t('frontend', 'Thank you for contacting us. We will respond to you as soon as possible. '),
                    'options'=>['class'=>'alert-success']
                ]);
                return $this->refresh();
            } else {
                print_r($model->errors);
                Yii::$app->getSession()->setFlash('alert', [
                    'body'=>\Yii::t('frontend', 'There was an error sending email. ') ,
                    'options'=>['class'=>'alert-danger']
                ]);
            }
        }

        return $this->render('contact-'.Lang::getCurrent()['url'], [
            'model' => $model
        ]);
    }


    public function actionAbout(){


        return $this->render("about-us-".Lang::getCurrent()['url']);
    }

    public function actionSitemap(){

        return $this->render("sitemap-".Lang::getCurrent()['url']);
    }

    public function actionSubscribe($email)
    {
        return $this->render('index'.Lang::getCurrent()['url']);
    }
}
