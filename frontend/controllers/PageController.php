<?php
/**
 * Created by PhpStorm.
 * User: zein
 * Date: 7/4/14
 * Time: 2:01 PM
 */

namespace frontend\controllers;

use Yii;
use common\models\Page;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use frontend\models\Lang;

class PageController extends Controller
{
    public function __construct($id,$module,$options = [])
    {
        parent::__construct($id,$module,$options);

        Lang::setCurrent(Yii::$app->request->url);
        \Yii::$app->language = Lang::getCurrent()['local'];

    }

    public function actionView($slug)
    {




        //\Yii::$app->language = "en-EN";
        $model = Page::find()->where(['slug'=>$slug, 'status'=>Page::STATUS_PUBLISHED, 'lang' => \Yii::$app->language])->one();
        if (!$model) {
            throw new NotFoundHttpException(Yii::t('frontend', 'Page not found'));
        }



        return $this->render('view', ['model'=>$model]);
    }
}
