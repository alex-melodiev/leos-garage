<?php
/**
 * Created by PhpStorm.
 * User: zein
 * Date: 7/4/14
 * Time: 2:25 PM
 */

namespace frontend\controllers;

use common\models\ArticleCategory;
use Yii;
use common\models\Article;
use yii\data\ActiveDataProvider;
use yii\helpers\StringHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use frontend\models\Lang;

class NewsController extends Controller
{
    public function __construct($id,$module,$options = [])
    {
        parent::__construct($id,$module,$options);

        Lang::setCurrent(Yii::$app->request->url);
        \Yii::$app->language = Lang::getCurrent()['local'];

    }

    public function actionIndex($cat = NULL)
    {
        if ($cat === NULL) {
            $dataProvider = new ActiveDataProvider([
                'query' => Article::find()->with('author')->published()->andWhere(['lang' => \Yii::$app->language])->orderBy(['published_at' => SORT_DESC])
            ]);
        }
        else
        {

            $category = ArticleCategory::find()->where(['slug' => $cat])->one();


            if ($category === NULL)
            {
                throw new NotFoundHttpException;
            }
            else
            {
                $cat_id = $category->id;
            }


            $dataProvider = new ActiveDataProvider([
                'query' => Article::find()->with('author')->published()->andWhere(['lang' => \Yii::$app->language,'category_id' => $cat_id])->orderBy(['published_at' => SORT_DESC])
            ]);

        }
        return $this->render('index', ['dataProvider'=>$dataProvider]);
    }

    public function actionView($id = null, $slug = null)
    {
	    if (!is_null($slug))
		    $model = Article::find()->published()->andWhere(['slug' => $slug])->andWhere(['lang' => \Yii::$app->language])->one();

	    if (!is_null($id)) {
		    $model = Article::find()->published()->andWhere(['id' => $id])->andWhere(['lang' => \Yii::$app->language])->one();
		    if ($model && $model->slug)
			    return $this->redirect(['view', 'slug' => $model->slug], 301);
	    }

        if (!$model) {
            throw new NotFoundHttpException;
        }

        $og_image = null;
	    $attachments = $model->articleAttachments;
	    if ($attachment = end($attachments)) {
            $og_image = $attachment->base_url . '/' . $attachment->path;
        }

        $og_description = strip_tags( StringHelper::truncateWords($model->body, 10));

        return $this->render('view', ['model'=>$model, 'og_image' => $og_image, 'og_description' => $og_description]);
    }
}
