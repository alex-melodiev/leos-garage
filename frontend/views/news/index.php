<?php
/* @var $this yii\web\View */
$this->title = Yii::t('frontend', 'News');
$this->slogan = Yii::t('frontend', 'Here’s the latest from Leo’s');
$this->subtitle = "At Leo’s, our vision is to be the garage of choice for car owners who know their cars.";
Yii::$app->params['body-class'][] = 'news-main-page';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="news-page pt-100">
    <div class="container">
    <?= \yii\widgets\ListView::widget([
        'dataProvider'=>$dataProvider,
        'pager'=>[
            'hideOnSinglePage'=>true,
        ],
        'itemView'=>'_item'
    ])?>
</div>
</div>