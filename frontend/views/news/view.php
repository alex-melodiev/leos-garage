<?php
/* @var $this yii\web\View */
/* @var $model common\models\Article */

$this->title = $model->title;
$this->slogan = $model->title;
$this->subtitle = "At Leo’s, our vision is to be your garage of choice.";

$this->og_image = $og_image;
$this->og_description = $og_description;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'News'), 'url' => ['index']];

Yii::$app->params['body-class'][] = 'news-main-page';
Yii::$app->params['body-class'][] = 'news-single-page';
?>

<div class="news-page pt-100">
    <div class="container">
        <div class="news-block mb-70px">
            <div class="news-meta mb-40px">
                <div class="news-city"><?= Yii::t('frontend', 'DUBAI')?></div>
                <div class="news-date"><?= Yii::$app->formatter->asDate($model->published_at, "php:d F") ?></div>
            </div>
            <!--news-meta-->
            <?php
            if (!empty($model->articleAttachments))
            {

            ?>
                <div class="news-image">
                    <?php
                         $attachments = $model->articleAttachments;
                    if (count($attachments)===1)
                    {
                        $attachment = array_shift($attachments);
                        echo \yii\helpers\Html::img($attachment->base_url . '/' . $attachment->path, ['class' => 'img-responsive']);
                    }
                    else {
                        ?>
                        <?php

                        $slide_items = array();

                        foreach ($attachments as $attachment) $slide_items[] = \yii\helpers\Html::img($attachment->base_url . '/' . $attachment->path, ['class' => 'img-responsive']);

                        ?>
                        <div id="owl-news" class="owl-carousel owl-theme">
                            <?php foreach ($slide_items as $slide) echo "<div>$slide</div>"; ?>
                        </div>

                        <?php


                        $this->registerJs(<<<JS
        var sync1;


        function init_owl_sync()
        {
            sync1 = $("#owl-news");


            sync1.owlCarousel({
                singleItem : true,
                slideSpeed : 1000,
                navigation: true,
                transitionStyle : "fade",
                responsiveRefreshRate : 200
            });

        }


        $(document).keyup(
            function (event)
            {
                if (event.keyCode == 39)
                {
                    sync1.trigger("owl.next");

                }

                if (event.keyCode == 37)
                {
                    sync1.trigger("owl.prev");
                }

            }
        );


                    init_owl_sync();
JS
                        );
//                    $this->registerJs('');
                    }
                    ?>

                </div>
        <?php
            }
            ?>
            <?= $model->body ?>
        </div>
        <!--news-block-->
        <span class='st_facebook_hcount' displayText='Facebook'></span>
        <span class='st_twitter_hcount' displayText='Tweet'></span>
        <span class='st_email_hcount' displayText='Email'></span>
    </div>
    <!--container-->
</div>
