<?php
/**
 * @var $this yii\web\View
 * @var $model common\models\Article
 */
use yii\helpers\Html;

?>
<div class="news-block mb-70px">
    <div class="news-meta mb-40px">
        <div class="news-city"><?= Yii::t('frontend', 'DUBAI')?></div>
        <div class="news-date"><?= Yii::$app->formatter->asDate($model->published_at, "php:d F") ?></div>

    </div>
    <h2 class="dark">
        <?= Html::a($model->title, ['view', 'slug' => $model->slug]) ?>
    </h2>
    <p> <?= $model->body ?></p>
	<?= Html::a(Yii::t('frontend', 'read more'), ['view', 'slug' => $model->slug], ['class' => 'btn btn-white']) ?>
</div><!--news-block-->

