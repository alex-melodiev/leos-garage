<?php
/**
 * @var $this \yii\web\View
 * @var $model \common\models\Page
 */
$this->title = $model->title;
$this->params['heading'] = $model->title;
$this->slogan = $model->slogan;
$this->subtitle = $model->vision;

if (trim($model->meta_title))
    $this->title = $model->meta_title;

if (trim($model->meta_keywords))
    $this->registerMetaTag(['name' => 'keywords', 'content' => $model->meta_keywords], 'keywords');

if (trim($model->meta_description))
    $this->registerMetaTag(['name' => 'description', 'content' => $model->meta_description], 'description');

$this->params['breadcrumbs'][] = $model->title;
?>
<div class="content">
    <div class="container">
        <div class="mb-70px">
            <?= $model->body ?>
        </div>
        <span class='st_facebook_hcount' displayText='Facebook'></span>
        <span class='st_twitter_hcount' displayText='Tweet'></span>
        <span class='st_email_hcount' displayText='Email'></span>
    </div><!--container-->
</div>