<?php
	/* @var $this \yii\web\View */
	use yii\helpers\ArrayHelper;
	use yii\widgets\Breadcrumbs;

	/* @var $content string */

	$this->beginContent('@frontend/views/layouts/contacts/_base.php')
?>
<div class="silver-bg">
	<div class="container">
		<?= Breadcrumbs::widget([
									'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
								]) ?>
		<?php if(Yii::$app->session->hasFlash('alert')):?>
			<?= \yii\bootstrap\Alert::widget([
												 'body'=>ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'body'),
												 'options'=>ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'options'),
											 ])?>
		<?php endif; ?>
	</div><!--container-->
</div>



<?= $content ?>


<?php $this->endContent() ?>
