<?php
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */
/* @var $content string */

\frontend\assets\FrontendAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
	     <?php
         if (isset($this->params['seo_title']))
         {
             echo $this->params['seo_title']; // если title передан на страницу жестко (для seo)
         }
         else
         {
             if (isset($this->meta_title) && strlen($this->meta_title) > 5) echo $this->meta_title;
             else echo $this->title . " " . \Yii::$app->keyStorage->get("frontend.template-title");
         }
        //echo ($this->title ?: Yii::$app->name);
	    //echo ' | The garage of choice for car owners who know their cars in Dubai | Body & Paint | Electrical Works | Mechanical Works | Chassis Repair in Dubai | Diagnostic Services | Tyre Services in Dubai | General Maintenance Services.';
	    ?>
    </title>
    <?php $this->head() ?>
	<link rel="icon" type="img/ico" href="<?=Url::base()?>/favicon.ico">
    <?= Html::csrfMetaTags() ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <meta property="og:title" content="Leo's Garage<?= (Url::to(['/']) != Url::to([Yii::$app->request->url]) ? " - " . Html::encode($this->title) : "") ?>">
    <meta property="og:site_name" content="Leo's Garage Dubai">
    <meta property="og:url" content="<?= Yii::$app->request->absoluteUrl ?>">
    <meta property="og:description" content="В Leo's, мы хотели бы быть любимым гаражом для тех автовладельцев, кто кое-что смыслит в своих машинах">
    <meta property="og:image" content="<?= (isset($this->og_image) ? $this->og_image : Url::to(['/images/avatar.jpg'], true)) ?>">
    <meta property="og:type" content="website">

</head>
<body class="rus-leos">

<?php $this->beginBody() ?>
<script type="text/javascript">var switchTo5x=true;</script>
<!--<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>-->
<script type="text/javascript">stLight.options({publisher: "e71c620a-e8e1-41db-bafc-87b9d7f066e9", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>

<div class="main">
    <div id="wrapper">
        <div id="sidebar">
            <div class="menu_contain">
                <a href="<?= Url::to(['/']) ?>" class="rs-link mb-35px">Наши сервисы</a>
                <a href="<?= Url::to(['/page/location']) ?>" class="rs-link mb-35px">Наше расположение</a>
                <a href="tel:<?= \Yii::$app->keyStorage->get("backend.phone-number"); ?>"
                   class="rs-link mb-35px leo"><?= \Yii::$app->keyStorage->get("frontend.phone-number"); ?></a>

                <div class="rs-list mb-60px">
                    <ul>
                        <li><a href="<?= Url::to(['/site/about']) ?>">About Us</a></li>
                        <li><a href="<?= Url::to(['/site/contact']) ?>">Contact Us</a></li>
                        <li><a href="<?= Url::to(['/news/index']) ?>">News</a></li>
                        <li><a href="<?= Url::to(['/page/faq']) ?>">FAQ</a></li>
                    </ul>
                </div>
                <!--rs-list-->
               <div class="rs-social">
                    <a href="https://www.facebook.com/1088808634467634" class="rs-f" target="_blank" rel="nofollow"></a>
                    <a href="https://twitter.com/LeosGarageAE" class="rs-t" target="_blank" rel="nofollow"></a>
                    <a href="https://instagram.com/LeosGarageAe" class="rs-i" target="_blank" rel="nofollow"></a>
                </div>
                <!--rs-social-->
            </div>
            <!--menu-contain-->
        </div>
        <div class="top-container">
            <header id="header">
                <div class="container-fluid">
                    <div class="header-search">
                        <form role="search" action="" method="get">
                            <div class="container clearfix">
                                <div class="s-icon"><i class="fa fa-search"></i></div>
                                <!--<input type="text" name="s" class="form-control" placeholder="Search Leo’s Garage">-->
                            </div>
                        </form>
                    </div>
                    <div class="row">
                        <div class="col-xs-6 col-sm-5 col-md-2 col-lg-2">
                            <a href="<?= Url::to(['/']) ?>" class="logo"><img
                                    src="<?= Url::to(['/images/logo.png']) ?>" alt=""></a>
                        </div>
                        <!--col-xs-2-->
                        <div
                            class="col-xs-6 col-sm-offset-3 col-sm-4 col-md-offset-8 col-md-2 col-lg-offset-8 col-lg-2">
                            <div class="hr-2">
                            <div class="header-language-block">
                                <a href="/">Eng</a>
                                <a href="/ru">Ru</a>
                            </div><!--header-language-block-->
                            <div class="header-right-block text-right">
                                <!-- <a href="#" class="search-control"><i class="fa fa-search"></i></a>-->
                                <a href="#" class="tcon tcon-menu--xbutterfly openMenu menu-header"
                                   aria-label="toggle menu" data-toggle="#wrapper" id="sidebar-toggle">
                                    <span class="tcon-menu__lines" aria-hidden="true"></span>
                                    <span class="tcon-visuallyhidden">переключить</span>
                                </a>
                            </div>
                            <!--header-right-block-->
                            </div>
                        </div>
                        <!--col-xs-2-->
                    </div>
                    <!--row-->
                </div>
                <!--container-fluid-->
            </header>
            <!--header-->
            <div class="top-block">
                <div class="container text-center">
                    <div class="col-xs-12 col-sm-offset-3 col-sm-6 col-md-offset-2 col-md-8 col-lg-offset-2 col-lg-8">
                        <div class="h1">Leo's Garage</div>

                        <div class="yellow-car mb-25px"><img class="img-responsive"
                                                             src="<?= Url::to(['/images/yellow-car.png']) ?>"
                                                             alt=""></div>
                        <div class="top-subtitle">В Leo’s полагают, что мы и есть тот самым автосервис, который выбирают владельцы автомобилей, имеющие представление о своих автомобилях, и понимающие, что автомобиль - не роскошь, а средство передвижения.</div>
                    </div>
                    <!--col-md-6-->
                </div>
                <!--container-->
            </div>
            <!--top-block-->
        </div>

        <div class="header-nav hidden-xs">
            <div class="container">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <!-- Brand and toggle get grouped for better mobile display -->

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                <li><a href="<?= Url::to(['/page/location']) ?>">Как нас найти</a></li>
                                <li>
                                    <a href="tel:<?= \Yii::$app->keyStorage->get("backend.phone-number"); ?>">
	                                    <?= \Yii::$app->keyStorage->get("frontend.phone-number"); ?>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <!-- /.navbar-collapse -->
                    </div>
                    <!-- /.container-fluid -->
                </nav>
            </div>
            <!--container-->
        </div>
        <!--header-nav-->
        <div class="header-nav xs-header visible-xs">
            <div id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">

                    <li class="hphone-xs">
	                    <a href="tel:<?= \Yii::$app->keyStorage->get("backend.phone-number"); ?>">Звони бесплатно: <?= \Yii::$app->keyStorage->get("frontend.phone-number"); ?>
                            <span> <?= Html::img(['/images/phone-red.png']); ?></span>
	                    </a></li>
                    <li><a href="<?= Url::to(['/page/location']) ?>">Наше местоположение <span> <?= Html::img(['/images/maptag.png']); ?></span></a></li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <section id="content">
            <div class="top-title-container text-center">

                <h1 class="top-title"><?php echo "Скоро открытие..."; ?></h1>
            </div>
            <!--top-title-container-->
            <?= $content ?>
        </section>


        <footer id="footer">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                       <div class="social-links mb-20px">
                            <a href="https://www.facebook.com/1088808634467634" class="facebook" target="_blank" rel="nofollow"></a>
                            <a href="https://twitter.com/LeosGarageAE" class="twiter" target="_blank" rel="nofollow"></a>
                            <a href="https://instagram.com/LeosGarageAe" class="instagram" target="_blank" rel="nofollow"></a>
                            <!--<a href="#" class="youtube"></a>-->
                        </div>
                        <!--social-links-->
                        <div class="copyright mb-20px">
                            &copy; <?= date("Y") ?> <?= \Yii::$app->keyStorage->get('frontend.copyright') ?>
                        </div>
                        <!--copyright frontend.site-email -->
	                    <div class="call-us cr">
		                    <p style="margin: 0px; color: rgb(51, 51, 51);">Бесплатный номер:</p>
		                    <a href="tel:<?= \Yii::$app->keyStorage->get("backend.phone-number"); ?>">
			                    800 LEO
			                    <small>(536)</small>
		                    </a>
	                    </div>
                        <div class="call-us mail-us">
	                        <a href="mailto:<?= \Yii::$app->keyStorage->get("frontend.site-email"); ?>"><?= \Yii::$app->keyStorage->get("frontend.site-email"); ?></a>
                        </div>
                    </div>
                    <!--col-md-3-->
                    <div class="col-xs-12 col-sm-3 col-lg-offset-3 col-lg-2">
                        <div class="footer-nav">
                            <div class="fn-title mb-30px">Компания</div>
                            <ul>
                                <li><a href="<?= Url::to(['/site/about']) ?>">О нас</a></li>
                                <li><a href="<?= Url::to(['/news/index']) ?>">Новости</a></li>
                                <li><a href="<?= Url::to(['/page/location']) ?>">Местоположение гаража</a></li>
                            </ul>
                        </div>
                        <!--footer-nav-->
                    </div>
                    <!--col-md-3-->
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-2">
                        <div class="footer-nav">
                            <div class="fn-title mb-30px">Сервис</div>
                            <ul>
                                <li><a href="<?= Url::to(['/']) ?>">Услуги</a></li>
                                <li><a href="<?= Url::to(['/page/faq']) ?>">FAQ</a></li>
                                <li><a href="<?= Url::to(['/site/contact']) ?>">Обратная связь</a></li>
                            </ul>
                        </div>
                        <!--footer-nav-->
                    </div>
                    <!--col-md-3-->
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-2">
                        <div class="footer-nav">
                            <div class="fn-title mb-30px">Правовая информация</div>
                            <ul>
                                <li><a href="<?= Url::to(['/page/privacy']) ?>">Защита персональных данных</a></li>
                                <li><a href="<?= Url::to(['/page/terms']) ?>">Положения &amp; условия</a>
                                </li>
                            </ul>
                        </div>
                        <!--footer-nav-->
                    </div>
                    <!--col-md-3-->
                </div>
                <!--row-->
                <a href="#header" class="to-top-button">Наверх</a>
            </div>
            <!--container-->
        </footer>
    </div>
</div>
<?php $this->endBody() ?>

<?php if (YII_ENV_PROD) : ?>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function()
    { (i[r].q=i[r].q||[]).push(arguments)}
        ,i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-62013650-1', 'auto');
    ga('send', 'pageview');
</script>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter32230979 = new Ya.Metrika({id:32230979, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]")
        { d.addEventListener("DOMContentLoaded", f, false); }
        else
        { f(); }

    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/32230979" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<?php endif ?>

</body>
</html>
<?php $this->endPage() ?>
