<?php
/* @var $this yii\web\View */
$this->title = 'Автосервис Leo\'s';
$this->params['seo_title'] = "Обслуживание и ремонт автомобилей. Автосервис в Дубае - Leo's Garage";
$this->slogan = "My guys have the know and the how.";
$this->subtitle = "At Leo’s, our vision is to be the garage of choice for car owners who know their cars..";
?>


<div class="top-cont-one">
        <div class="row">
        <div class="col-sm-12 col-xs-6">
            <div class="body-paint-container hidden-xs">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                            <div class="bp-block">
                                 <h2 class="dark">Кузов и Покраска</h2>
                                <p class="sb-container dark">Будь то царапины на двери после поездки в торговый центр, или последствия того, что Вы забыли, какой педалью что делать ... (WOW!!!) - мы продемонстрируем, что Ваш автомобиль по-прежнему выглядит так, как это было раньше, - до того, как... (WOW!!!)  </p>
                                <a href="/ru/service/car-body-repairing" class="btn btn-read">Ремонт кузова</a>
                                <a href="/ru/service/car-painting-service" class="btn btn-read">Покраска</a>
                            </div>
                        </div><!--col-md-4-->
                       <div class="col-xs-12 col-sm-6 col-md-offset-2 col-md-6 col-md-offset-2 col-md-6 col-lg-offset-2 col-lg-6">
                            <div class="bp-image">
                                <img src="images/nissan.jpg" class="img-responsive" alt="" />
                            </div><!--bp-image-->
                        </div><!--col-md-6-->
                    </div><!--row-->
                </div><!--container-->
            </div><!--body-paint-container-->
            <div class="visible-xs">
                <a href="/ru/service/car-painting-service" class="xs-blocks nbt" id="xs1">
                    <div class="xsb-img">
                        <img src="images/body2.jpg" alt="Leos Garage - Покраска" class="img-responsive">
                    </div><!--xsb-img-->
                    <h2 class="dark">Покраска</h2>
                </a>
            </div><!--visible-xs-->
        </div><!--col-sm-12-->
        <div class="col-sm-12 col-xs-6">
            <div class="electric-works-container hidden-xs">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                            <div class="ew-block">
                                <h2 class="light">Работы по электрике</h2>
                                <p class="sb-container light">Если на Вашей панели приборов загорелся и мигает тревожный значок, не волнуйтесь: нашим специалистам известны все возможные варианты того, как это исправить.  </p>
                                <a href="/ru/service/car-electric-repair" class="btn btn-read">Прочитать больше</a>
                            </div><!--ew-block-->
                        </div><!--col-md-5-->
                        <div class="col-xs-12 col-sm-offset-1 col-sm-6 col-md-offset-1 col-lg-offset-1 col-md-6 col-lg-6 hidden-xs">
                            <div class="ew-image">
                                <img src="images/electr.jpg" alt="" class="img-responsive" />
                            </div><!--ew-image-->
                        </div><!--col-md-6-->
                    </div><!--row-->
                </div><!--container-->
            </div><!--electric-works-container-->
            <div class="visible-xs">
                <a href="/ru/service/car-electric-repair" class="xs-blocks nbt nbl" id="xs2">
                    <div class="xsb-img">
                        <img src="images/ew.jpg" alt="Leos Garage - Работы по электрике" class="img-responsive">
                    </div><!--xsb-img-->
                    <h2 class="dark">Работы по электрике</h2>
                </a>
            </div><!--visible-xs-->
        </div><!--col-sm-12-->
        <div class="col-sm-12 col-xs-6">
            <div class="mech-works hidden-xs">
                <div class="container">
                     <div class="mw-image mb-40px"></div>
        <h2 class="dark">Работы по механике</h2>
        <p class="sb-container dark">Все, что не связано с электросистемами автомобиля, требует участия наших механиков. Они такие же специалисты, как и электрики, но не работают под напряжением. </p>
        <a href="/ru/service/mechanical-works" class="btn btn-read">Прочитать больше</a>
                </div><!--container-->
            </div><!--mech-works-->
            <div class="visible-xs">
                <a href="/ru/service/mechanical-works" class="xs-blocks nbt" id="xs3">
                    <div class="xsb-img">
                        <img src="images/engine2.jpg" alt="Leos Garage - Работы по механике" class="img-responsive">
                    </div><!--xsb-img-->
                    <h2 class="dark">Работы по механике</h2>
                </a>
            </div><!--visible-xs-->
        </div><!--col-xs-6-->
        <div class="col-sm-12 col-xs-6">
            <div class="chasis hidden-xs">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-offset-1 col-xs-10 col-sm-offset-5 col-sm-6 col-md-offset-5 col-md-4 col-lg-4 col-lg-offset-5">
                          <h2 class="dark">Ремонт Шасси</h2>

                        <p class="sb-container dark text-overlay">Это любопытное французское слово – “chassis” ... . Но отставим лингвистику в сторону, попробуйте поверить нашим экспертам. Для них «шасси» на любом языке – это основа автомобиля. </p>
                        <a href="/ru/service/chassis-repair" class="btn btn-read">Прочитать больше</a>
                        </div><!--col-md-6-->
                    </div><!--row-->
                </div><!--container-->
            </div><!--chasis-->
            <div class="visible-xs">
                <a href="/ru/service/chassis-repair" class="xs-blocks nbt nbl" id="xs4">
                    <div class="xsb-img">
                        <img src="images/chassis2.jpg" alt="Leos Garage - Ремонт Шасси" class="img-responsive">
                    </div><!--xsb-img-->
                    <h2 class="dark">Ремонт Шасси</h2>
                </a>
            </div><!--visible-xs-->
        </div><!--col-xs-6-->
        <div class="col-sm-12 col-xs-6">
            <div class="car-interior text-right hidden-xs">
                <div class="container">
                    <h2 class="dark">Чистка салона</h2>
                    <p class="sb-container dark">Случайно пролили в салоне кофе или сок? Или Вас огорчила Ваша собака? Не расстраивайтесь, все очень просто. За одну ночь мы все исправим, а утром Вы поедете на работу уже в чистой машине.</p>
                    <a href="/ru/service/car-interior-cleaning" class="btn btn-read">Чистка Салона</a>
                    <a href="/ru/service/car-upholstery-cleaning" class="btn btn-read">Чистка обивки</a>
                </div><!--container-->
            </div>
            <div class="visible-xs">
                <a href="/ru/service/car-interior-cleaning" class="xs-blocks nbt" id="xs5">
                    <div class="xsb-img">
                        <img src="images/clean2.jpg" alt="Leos Garage - Чистка салона" class="img-responsive">
                    </div><!--xsb-img-->
                    <h2 class="dark">Чистка салона</h2>
                </a>
            </div><!--visible-xs-->
        </div><!--col-xs-6-->
        <div class="col-sm-12 col-xs-6">
            <div class="diagnostic-services hidden-xs">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-offset-6 col-sm-6 col-md-offset-7 col-md-5 col-lg-offset-7 col-lg-5">
                              <h2 class="light">Диагностический сервис</h2>
                                <p class="sb-container light text-overlay-dark">В больницах есть врачи, у нас - инженеры. Основа нашей работы – тоже диагностика, пусть мы и не носим белых халатов (хотя это было бы замечательно). И  у нас другие компьютеры, специальные, для выявления проблем с системами автомобиля.</p>
                                <a href="/ru/service/car-diagnostic-services" class="btn btn-read">Прочитать больше</a>
                        </div><!--col-lg-5-->
                    </div><!--row-->
                </div><!--container-->
            </div><!--diagnostic-services-->
            <div class="visible-xs">
                <a href="/ru/service/car-diagnostic-services" class="xs-blocks darker nbt nbl" id="xs6">
                    <div class="xsb-img">
                        <img src="images/car2.jpg" alt="Leos Garage - Диагностический сервис" class="img-responsive">
                    </div><!--xsb-img-->
                    <h2 class="dark">Диагностический сервис</h2>
                </a>
            </div><!--visible-xs-->
        </div><!--col-xs-6-->
        <div class="col-sm-12 col-xs-6">
            <div class="tyre hidden-xs">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-offset-3 col-sm-6 col-md-offset-3 col-md-6 col-lg-offset-3 col-lg-6">
                           <h2 class="dark">Шиномонтаж</h2>
                            <p class="sb-container dark mb-35px">Так же, как и Вашей обуви, колесам автомобиля необходимо внимание. Шины нуждаются в  отдельном  уходе, чтобы сохранить впечатление от поездки - «ровным и гладким». </p>
                            <a href="/ru/service/tyre-services" class="btn btn-read">Прочитать больше</a>
                        </div><!--col-lg-6-->
                    </div><!--row-->
                    <div class="shina"></div>
                </div><!--container-->
            </div><!--tyre-->
            <div class="visible-xs">
                <a href="/service/tyre-services" class="xs-blocks" id="xs7">
                    <div class="xsb-img">
                        <img src="images/tire2.jpg" alt="Leos Garage - Шиномонтаж" class="img-responsive">
                    </div><!--xsb-img-->
                    <h2 class="dark">Шиномонтаж</h2>
                </a>
            </div><!--visible-xs-->
        </div><!--col-xs-6-->
        <div class="col-sm-12 col-xs-6">
            <div class="general-ms hidden-xs">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-5 col-md-6 col-lg-6">
                             <h2 class="dark">Общее техническое обслуживание </h2>
                            <p class="sb-container dark">«Правильное содержание автомобиля» означает несколько больше, чем  полный топливный бак. Автомобиль может жить и работать 120 лет. Только в этом случае ему необходимо постоянное профессиональное обслуживание и подлинные запчасти. У нас есть все, чтобы Ваш автомобиль был в наилучшей форме. </p>
                            <a href="/ru/service/general-maintenance-car-services" class="btn btn-read">Прочитать больше</a>
                        </div><!--col-md-5-->
                    </div><!--row-->
                </div><!--container-->
            </div><!--general-ms-->
            <div class="visible-xs">
                <a href="/ru/service/general-maintenance-car-services" class="xs-blocks nbl" id="xs8">
                    <div class="xsb-img">
                        <img src="images/parts2.jpg" alt="Leos Garage - Общее техническое обслуживание" class="img-responsive">
                    </div><!--xsb-img-->
                    <h2 class="dark">Общее техническое обслуживание</h2>
                </a>
            </div><!--visible-xs-->
        </div><!--col-xs-6-->
        <div class="col-sm-12 col-xs-12">
            <div class="get-the-latest">
                <div class="container">
                    <!-- Begin MailChimp Signup Form -->
                    <div id="mc_embed_signup">
                        <form action="//leosgarage.us10.list-manage.com/subscribe/post?u=aa6fb896b545bddad66e25dd1&amp;id=02ccbcbc0c" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-offset-2 col-md-3 col-lg-offset-1 col-lg-3 text-right">
                                    <div class="get-title">Подпишитесь на наши новости</div>
                                </div><!--col-lg-4-->
                                <div class="col-xs-8 col-sm-4 col-md-3 col-lg-3">
                                    <div class="get-input">
                                        <input type="email" class="form-control" name="EMAIL" placeholder="Ваш Email">
                                    </div><!--get-input-->
                                </div><!--col-md-4-->
                                <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2">
                                    <div id="mce-responses" class="clear">
                                        <div class="response" id="mce-error-response" style="display:none"></div>
                                        <div class="response" id="mce-success-response" style="display:none"></div>
                                    </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                    <div style="position: absolute; left: -5000px;"><input type="text" name="b_aa6fb896b545bddad66e25dd1_02ccbcbc0c" tabindex="-1" value=""></div>
                                    <input type="submit" value="Подписаться" name="subscribe" id="mc-embedded-subscribe" class="btn btn-white">
                                </div><!--col-md-2-->
                            </div><!--row-->
                        </form>
                    </div>
                    <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>


                    <!--End mc_embed_signup-->
                </div><!--container-->
            </div><!--get-the-latest-->
        </div><!--col-xs-6-->
    </div><!--row-->
</div>
