<?php
/* @var $this yii\web\View */
$this->title = \Yii::t("about", "Story");
$this->slogan = \Yii::t("about", "Here’s my pit crew.");
$this->subtitle = \Yii::t("layout", "At Leo’s, our vision is to be your garage of choice.");
?>
<div class="about-us pt-100">
    <div class="container">
        <div class="our-story mb-70px">
            <div class="page-title mb-20px"><?= \Yii::t("about", "Our story") ?></div>
            <p><?= \Yii::t("about", "In our world, a car mechanic is like a doctor – he needs to have the knowledge to make a diagnosis and have impeccable bedside manner. And we’d like to think we have that, but don’t take our word for it. From the Ford Model-T to the latest iteration of the Bull, the DNA of cars hasn't changed since their creation. At Leo’s, we consider ourselves not just Dubai’s new car experts, but true car enthusiasts so there’s very (very very) little we can’t cure . Some of us may sound, say, exotic but we know how to speak with cars in any language.") ?></p>
        </div><!--our-story-->
    </div><!--container-->
</div><!--about-us-->