<?php
/* @var $this yii\web\View */
$this->title = Yii::$app->name;
$this->params['seo_title'] = "Car repairning service - Leo's Garage in Dubai";
$this->slogan = "My guys have the know and the how.";
$this->subtitle = "At Leo’s, our vision is to be the garage of choice for car owners who know their cars.";
?>


<?php //echo \common\components\widgets\DbCarousel::widget([
//        'key'=>'index'
//    ]) ?>



<?php //echo common\components\widgets\DbMenu::widget([
//            'key'=>'frontend-index',
//            'options'=>[
//                'tag'=>'p'
//            ]
//        ]) ?>
    <div class="row">
        <div class="col-sm-12 col-xs-6">
            <div class="body-paint-container">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                            <div class="bp-block">
                                <h2 class="dark">Body & Paint</h2>
                                <p class="sb-container dark">Whether it’s a door scratch after a trip to the mall, or you forgot which pedal is which (whoops), we’ll make sure your car looks the way it was, regardless of nail polish colour.</p>
                                <a href="/service/car-body-repairing" class="btn btn-read">Body Repairing</a>
                                <a href="/service/car-painting-service" class="btn btn-read">Paint Service</a>
                            </div>
                        </div><!--col-md-4-->
                        <div class="col-xs-12 col-sm-6 col-md-offset-2 col-md-6 col-md-offset-2 col-md-6 col-lg-offset-2 col-lg-6">
                            <div class="bp-image">
                                <img src="images/black-car.jpg" class="img-responsive" alt="" />
                            </div><!--bp-image-->
                        </div><!--col-md-6-->
                    </div><!--row-->
                </div><!--container-->
            </div><!--body-paint-container-->
        </div><!--col-sm-12-->
        <div class="col-sm-12 col-xs-6">
            <div class="electric-works-container">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                            <div class="ew-block">
                                <h2 class="light">Electrical Works</h2>
                                <p class="sb-container light">If it used to blink, flash, light up or swing open, it probably stopped due to an electrical malfunction. Don’t worry, our electricians know the positives and negatives.</p>
                                <a href="/service/car-electric-repair" class="btn btn-read">Read More</a>
                            </div><!--ew-block-->
                        </div><!--col-md-5-->
                        <div class="col-xs-12 col-sm-offset-1 col-sm-6 col-md-offset-1 col-lg-offset-1 col-md-6 col-lg-6 hidden-xs">
                            <div class="ew-image">
                                <img src="images/ew.jpg" alt="" class="img-responsive" />
                            </div><!--ew-image-->
                        </div><!--col-md-6-->
                    </div><!--row-->
                </div><!--container-->
            </div><!--electric-works-container-->
        </div><!--col-sm-12-->
        <div class="col-sm-12 col-xs-6">
            <div class="mech-works">
                <div class="container">
                    <div class="mw-image mb-40px"></div>
                    <h2 class="dark">Mechanical Works</h2>
                    <p class="sb-container dark">Anything that doesn’t have an electric connection calls for our mechanics. They’re just like the electricians, but without the charge. </p>
                    <a href="/service/mechanical-works" class="btn btn-read">Read More</a>
                </div><!--container-->
            </div><!--mech-works-->
        </div><!--col-xs-6-->
        <div class="col-sm-12 col-xs-6">
            <div class="chasis">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-offset-1 col-xs-10 col-sm-offset-5 col-sm-6 col-md-offset-5 col-md-4 col-lg-4 col-lg-offset-5">
                            <h2 class="dark">Chassis Repair</h2>
                            <p class="sb-container dark">It’s a curious word.. ‘Chassis’. Almost sounds French. Linguistics aside, trust our experts. They know their chassis in any language.</p>
                            <a href="/service/chassis-repair" class="btn btn-read">Read More</a>
                        </div><!--col-md-6-->
                    </div><!--row-->
                </div><!--container-->
            </div><!--chasis-->
        </div><!--col-xs-6-->
        <div class="col-sm-12 col-xs-6">
            <div class="car-interior text-right">
                <div class="container">
                    <h2 class="dark">Car Interior Cleaning</h2>
                    <p class="sb-container dark">Spilled coffee or juice?<br><br>It doesn’t matter, we’ll clean it over-night using special equipment!<br>That’s right, drop the car off in the evening and pick it up<br>before work, simple. </p>
                    <a href="/service/car-interior-cleaning" class="btn btn-read">Interior cleaning</a>
                    <a href="/service/car-upholstery-cleaning" class="btn btn-read">upholstery cleaning</a>
                </div><!--container-->
            </div>
        </div><!--col-xs-6-->
        <div class="col-sm-12 col-xs-6">
            <div class="diagnostic-services">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-offset-6 col-sm-6 col-md-offset-7 col-md-5 col-lg-offset-7 col-lg-5">
                            <h2 class="light">Diagnostic Services</h2>
                            <p class="sb-container light">Hospitals have doctors, we have our engineers. We’re diagnosticians, even though we don’t wear white lab coats (though that would be cool). The difference is that we also have computers to help us identify the problem.</p>
                            <a href="/service/car-diagnostic-services" class="btn btn-read">Read More</a>
                        </div><!--col-lg-5-->
                    </div><!--row-->
                </div><!--container-->
            </div><!--diagnostic-services-->
        </div><!--col-xs-6-->
        <div class="col-sm-12 col-xs-6">
            <div class="tyre">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-offset-3 col-sm-6 col-md-offset-3 col-md-6 col-lg-offset-3 col-lg-6">
                            <h2 class="dark">Tyre Services</h2>
                            <p class="sb-container dark mb-35px">Just like feet deserve the best care to keep you running, tyres need their own type of treatment to keep the journey smooth.</p>
                            <a href="/service/tyre-services" class="btn btn-read">Read More</a>
                        </div><!--col-lg-6-->
                    </div><!--row-->
                    <div class="shina"></div>
                </div><!--container-->
            </div><!--tyre-->
        </div><!--col-xs-6-->
        <div class="col-sm-12 col-xs-6">
            <div class="general-ms">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-5 col-md-6 col-lg-6">
                            <h2 class="dark">General Maintenance Services</h2>
                            <p class="sb-container dark">Proper car upkeep means more than keeping the tank full. That’s where we come in. We have everything your car would want in order to be in the best possible shape.</p>
                            <a href="/service/general-maintenance-car-services" class="btn btn-read">Read More</a>
                        </div><!--col-md-5-->
                    </div><!--row-->
                </div><!--container-->
            </div><!--general-ms-->
        </div><!--col-xs-6-->
        <div class="col-sm-12 col-xs-6">
            <div class="get-the-latest">
                <div class="container">
                    <!-- Begin MailChimp Signup Form -->
                    <div id="mc_embed_signup">
                        <form action="//leosgarage.us10.list-manage.com/subscribe/post?u=aa6fb896b545bddad66e25dd1&amp;id=02ccbcbc0c" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                            <div class="row">
                                <div class="col-xs-12 col-sm-4 col-md-offset-2 col-md-3 col-lg-offset-1 col-lg-3 text-right">
                                    <div class="get-title">Get the latest from Leo’s</div>
                                </div><!--col-lg-4-->
                                <div class="col-xs-8 col-sm-4 col-md-3 col-lg-3">
                                    <div class="get-input">
                                        <input type="email" class="form-control" name="EMAIL" placeholder="Your E-mail here">
                                    </div><!--get-input-->
                                </div><!--col-md-4-->
                                <div class="col-xs-4 col-sm-4 col-md-2 col-lg-2">
                                    <div id="mce-responses" class="clear">
                                        <div class="response" id="mce-error-response" style="display:none"></div>
                                        <div class="response" id="mce-success-response" style="display:none"></div>
                                    </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                    <div style="position: absolute; left: -5000px;"><input type="text" name="b_aa6fb896b545bddad66e25dd1_02ccbcbc0c" tabindex="-1" value=""></div>
                                    <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="btn btn-white">
                                </div><!--col-md-2-->
                            </div><!--row-->
                        </form>
                    </div>
                    <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>


                    <!--End mc_embed_signup-->
                </div><!--container-->
            </div><!--get-the-latest-->
        </div><!--col-xs-6-->
    </div><!--row-->



