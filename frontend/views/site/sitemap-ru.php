<?php
/**
 * Created by PhpStorm.
 * User: dusty
 * Date: 09.12.15
 * Time: 10:36
 */
$this->title = "Карта сайта";

$this->subtitle = "Карта для навигации по сайту";
?>
<div class="sitemap pt-100">
    <div class="container">
        <div class="sitemap mb-70px">
            <div class="page-title mb-20px">Карта сайта</div>
            <ul class="sitemap sitemap-1st">
                <li>Услуги
                    <ul class="sitemap sitemap-2nd">
                        <li><a href="/ru/service/car-body-repairing">Ремонт кузова</a></li>
                        <li><a href="/ru/service/car-painting-service">Покраска авто</a></li>
                        <li><a href="/ru/service/car-electric-repair">Работы по электрике</a></li>
                        <li><a href="/ru/service/mechanical-works">Работы по механике</a></li>
                        <li><a href="/ru/service/chassis-repair">Ремонт ходовой/подвеска</a></li>
                        <li><a href="/ru/service/car-interior-cleaning">Чистка салона</a></li>
                        <li><a href="/ru/service/car-upholstery-cleaning">Чистка обивки</a></li>
                        <li><a href="/ru/service/car-diagnostic-services">Диагностика</a></li>
                        <li><a href="/ru/service/tyre-services">Шиномонтаж</a></li>
                        <li><a href="/ru/service/general-maintenance-car-services">Техническое обслуживание автомобиля</a></li>
                    </ul>
                </li>
                <li>Компания
                    <ul class="sitemap sitemap-2nd">
                        <li><a href="/ru/site/about">О нас</a></li>
                        <li><a href="/ru/site/contact">Обратная связь</a></li>
                        <li><a href="/ru/page/faq">FAQ</a></li>
                        <li><a href="/ru/page/location">Местоположение гаража</a></li>
                    </ul>
                </li>
                <li><a href="/news">Новости</a></li>
                <li>Правовая информация
                    <ul class="sitemap sitemap-2nd">
                        <li><a href="/ru/page/privacy">Защита персональных данных</a></li>
                        <li><a href="/ru/page/terms">Положения & условия</a></li>
                    </ul>
                </li>
            </ul>
        </div><!--our-story-->
    </div><!--container-->
</div><!--about-us-->
