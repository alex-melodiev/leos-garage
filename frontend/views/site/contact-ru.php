<?php
	use yii\helpers\Html;
	use yii\widgets\ActiveForm;

	/* @var $this yii\web\View */
	/* @var $form yii\widgets\ActiveForm */
	/* @var $model \frontend\models\ContactForm */
	$this->title    = \Yii::t("contacts", "Contacts");
	$this->slogan   = \Yii::t("contacts", 'Can’t I just text?');
	$this->subtitle = \Yii::t("layout", "At Leo’s, our vision is to be the garage of choice for car owners who know their cars.");
	//$this->params['breadcrumbs'][] = $this->title;

?>
<div class="contact-us">
	<div class="container">
		<div class="contact-us-block">
			<div class="contact-us-subtitle mb-40px"><?=\Yii::t("contacts", "We’d love to hear from your car but since we’re still teaching them to speak, drop us a line on their behalf.")?>
			</div>
			<div class="contact-form">
				<?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>


				<?=$form->field($model, 'name', [
						'options'  => ['class' => 'row mb-30px'],
						'template' => "<div class='col-sm-4 col-md-4 col-lg-4'>{label}</div><div class='col-sm-8 col-md-8 col-lg-8'>{input}</div>\n{hint}\n<div class='col-sm-offset-4 col-md-offset-4 col-lg-offset-4 col-sm-8 col-md-8 col-lg-8'>{error}</div>",
				])
				        ->textInput(['maxlength' => 100])
				        ->label('Ваше Имя')?>

				<?=$form->field($model, 'email', [
						'options'  => ['class' => 'row mb-30px'],
						'template' => "<div class='col-sm-4 col-md-4 col-lg-4'>{label}</div><div class='col-sm-8 col-md-8 col-lg-8'>{input}</div>\n{hint}\n<div class='col-sm-offset-4 col-md-offset-4 col-lg-offset-4 col-sm-8 col-md-8 col-lg-8'>{error}</div>",
				])
				        ->textInput(['maxlength' => 100])
				        ->label('Email')?>

				<?=$form->field($model, 'phone', [
						'options'  => ['class' => 'row mb-30px'],
						'template' => "<div class='col-sm-4 col-md-4 col-lg-4'>{label}</div><div class='col-sm-8 col-md-8 col-lg-8'>{input}</div>\n{hint}\n<div class='col-sm-offset-4 col-md-offset-4 col-lg-offset-4 col-sm-8 col-md-8 col-lg-8'>{error}</div>",
				])
				        ->textInput(['maxlength' => 100])
				        ->label('Телефон')?>


				<?=$form->field($model, 'subject', [
						'options'  => ['class' => 'row mb-30px'],
						'template' => "<div class='col-sm-4 col-md-4 col-lg-4'>{label}</div><div class='col-sm-8 col-md-8 col-lg-8'>{input}</div>\n{hint}\n<div class='col-sm-offset-4 col-md-offset-4 col-lg-offset-4 col-sm-8 col-md-8 col-lg-8'>{error}</div>",
				])
				        ->textInput(['maxlength' => 100])
				        ->label('Тема')?>


				<?=$form->field($model, 'body', [
						'options'  => ['class' => 'row mb-30px'],
						'template' => "<div class='col-sm-4 col-md-4 col-lg-4'>{label}</div><div class='col-sm-8 col-md-8 col-lg-8'>{input}</div>\n{hint}\n<div class='col-sm-offset-4 col-md-offset-4 col-lg-offset-4 col-sm-8 col-md-8 col-lg-8'>{error}</div>",
				])
				        ->textarea(['rows' => 6])
				        ->label('Сообщение')?>

				<div class="row">
					<div class="col-sm-offset-4 col-md-offset-4 col-lg-offset-4 col-sm-8 col-md-8 col-lg-8">
						<div class="form-group">
							<?=Html::submitButton(Yii::t('frontend', 'Submit'), [
									'class' => 'btn btn-white',
									'name'  => 'contact-button',
							])?>
						</div>
					</div>
				</div>
				<?php ActiveForm::end(); ?>
			</div>
			<!--form-->
		</div>
		<!--contact-us-block-->
	</div>
	<!--container-->
	<div class="get-in">
		<div class="container text-center">
			<div class="c-mail"><?=\Yii::t("contacts", "Contact us directly")?>:
				<a href="mailto:fixit@leosgarage.ae">fixit@leosgarage.ae</a>
			</div>
		</div>
		<!--container-->
	</div>
	<!--get-in-->
</div><!--contact-us-->
