<?php
/**
 * Created by PhpStorm.
 * User: dusty
 * Date: 09.12.15
 * Time: 10:36
 */
$this->title = "Sitemap";

$this->subtitle = "At Leo’s, our vision is to be your garage of choice.";
?>
<div class="sitemap pt-100">
    <div class="container">
        <div class="sitemap mb-70px">
            <div class="page-title mb-20px">Sitemap</div>
            <ul class="sitemap sitemap-1st">
                <li>Services
                    <ul class="sitemap sitemap-2nd">
                        <li><a href="/service/car-body-repairing">Body Repair</a></li>
                        <li><a href="/service/car-painting-service">Painting Service</a></li>
                        <li><a href="/service/car-electric-repair">Electrical Works</a></li>
                        <li><a href="/service/mechanical-works">Mechanical Works</a></li>
                        <li><a href="/service/chassis-repair">Chassis Repair</a></li>
                        <li><a href="/service/car-interior-cleaning">Interior Cleaning</a></li>
                        <li><a href="/service/car-upholstery-cleaning">Upholstery Cleaning</a></li>
                        <li><a href="/service/car-diagnostic-services">Diagnostic Services</a></li>
                        <li><a href="/service/tyre-services">Tyre Services</a></li>
                        <li><a href="/service/general-maintenance-car-services">General Maintenance Services</a></li>
                    </ul>
                </li>
                <li>Company
                    <ul class="sitemap sitemap-2nd">
                        <li><a href="/site/about">About Us</a></li>
                        <li><a href="/site/contact">Contact Us</a></li>
                        <li><a href="/page/faq">FAQ</a></li>
                        <li><a href="/page/location">Garage Location</a></li>
                    </ul>
                </li>
                <li><a href="/news">News</a></li>
                <li>Legal
                    <ul class="sitemap sitemap-2nd">
                        <li><a href="/page/privacy">Privacy</a></li>
                        <li><a href="/page/terms">Terms & Conditions</a></li>
                    </ul>
                </li>
            </ul>
        </div><!--our-story-->
    </div><!--container-->
</div><!--about-us-->