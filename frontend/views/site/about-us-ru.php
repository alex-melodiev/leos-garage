<?php
/* @var $this yii\web\View */
$this->title = \Yii::t("about", "Story");
$this->slogan = \Yii::t("about", "Here’s my pit crew.");
$this->subtitle = \Yii::t("layout", "At Leo’s, our vision is to be your garage of choice.");
?>
<div class="about-us pt-100">
    <div class="container">
        <div class="our-story mb-70px">
          <div class="page-title mb-20px">Наша история</div>
            <p>В нашем мире автомеханик, как и врач, должен иметь знания и опыт, чтобы поставить диагноз, исправить, отрегулировать системы и устройства автомобиля, и при этом иметь безупречный инженерный такт. И мы полагаем, что это у нас есть. 
Мир автомобилей становится все больше и сложнее,  но ДНК автомобилей не изменилась с момента их создания. Мы считаем себя экпертами по автомобилям в Дубае, и существует очень небольшое число автомобилей, которым мы не сможем помочь. Хотя некоторые автомобили достаточно редкие и экзотичные, но мы все равно можем найти с ними общий язык. 
</p></div><!--our-story-->
    </div><!--container-->
</div><!--about-us-->