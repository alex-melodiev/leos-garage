<?php
/* @var $this yii\web\View */
    $this->title = "About Us";
    $this->slogan = "Here’s my pit crew.";
    $this->subtitle = "At Leo’s, our vision is to be your garage of choice.";
?>
<div class="about-us pt-100">

    <div class="container">
        <div class="our-story mb-70px">
            <div class="page-title mb-20px">Our story</div>
            <p>In our world, a car mechanic is like a doctor – he needs to have the knowledge to make a diagnosis and have impeccable bedside manner. And we’d like to think we have that, but don’t take our word for it. From the Ford Model-T to the latest iteration of the Bull, the DNA of cars hasn't changed since their creation. At Leo’s, we consider ourselves not just Dubai’s new car experts, but true car enthusiasts so there’s very (very very) little we can’t cure . Some of us may sound, say, exotic but we know how to speak with cars in any language.</p>
        </div><!--our-story-->
    </div><!--container-->
    <div class="our-image mb-70px">
        <img src="<?= \yii\helpers\Url::to(['/images/dubai.jpg'])?>" alt="" />
    </div><!--our-image-->
    <div class="we-leos text-center mb-70px">
        <div class="page-title mb-50px">We're Leos</div>
        <div class="we-photo"><img src="<?= \yii\helpers\Url::to(['/images/we.jpg'])?>" alt="" /></div>
    </div><!--we-leos-->
    <div class="leadership-block">
        <div class="container">
            <div class="page-title mb-50px">Leadership</div>
            <div class="lb-user mb-70px">
                <div class="lb-photo mb-35px"><img src="<?= \yii\helpers\Url::to(['/images/lb1.jpg'])?>" alt="" class="img-responsive"/></div>
                <div class="row">
                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <div class="lb-name">Dima Bilan</div>
                        <div class="lb-job">General Manager</div>
                    </div><!--col-md-4-->
                    <div class="col-sm-9 col-md-9 col-lg-9">
                        <p>Fusce a arcu urna. Duis eu augue euismod, mollis magna at, lobortis justo. Proin ex quam, tempus in ipsum vitae, suscipit rhoncus tellus. Sed ac tincidunt magna. Donec ultricies augue ac libero molestie porttitor. Curabitur sed pretium ex. Vestibulum sit amet tincidunt justo. In vulputate cursus tincidunt. Phasellus id ultrices sem. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Etiam magna ante, egestas quis ante ut, bibendum gravida quam. Aenean mollis dolor ac libero eleifend tincidunt. Pellentesque a sem a ipsum finibus facilisis. Morbi faucibus at orci ut auctor.</p>
                    </div><!--col-md-8-->
                </div><!--row-->
            </div><!--lb-user-->
            <div class="lb-user mb-70px">
                <div class="lb-photo mb-35px"><img src="<?= \yii\helpers\Url::to(['/images/lb1.jpg'])?>" alt="" class="img-responsive"/></div>
                <div class="row">
                    <div class="col-sm-3 col-md-3 col-lg-3">
                        <div class="lb-name">Dima Bilan</div>
                        <div class="lb-job">General Manager</div>
                    </div><!--col-md-4-->
                    <div class="col-sm-9 col-md-9 col-lg-9">
                        <p>Fusce a arcu urna. Duis eu augue euismod, mollis magna at, lobortis justo. Proin ex quam, tempus in ipsum vitae, suscipit rhoncus tellus. Sed ac tincidunt magna. Donec ultricies augue ac libero molestie porttitor. Curabitur sed pretium ex. Vestibulum sit amet tincidunt justo. In vulputate cursus tincidunt. Phasellus id ultrices sem. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Etiam magna ante, egestas quis ante ut, bibendum gravida quam. Aenean mollis dolor ac libero eleifend tincidunt. Pellentesque a sem a ipsum finibus facilisis. Morbi faucibus at orci ut auctor.</p>
                    </div><!--col-md-8-->
                </div><!--row-->
            </div><!--lb-user-->
        </div><!--container-->
    </div><!--leadeship-block-->

    <div class="team-block">
        <div class="container">
            <div class="page-title mb-50px">Team</div>
        </div><!--container-->
        <div class="flipster">
            <ul >
                <li>
                    <div class="team-block">
                        <div class="team-image mb-20px">
                            <img src="<?= \yii\helpers\Url::to(['/images/s1.jpg'])?>" alt="" />
                        </div><!--team-image-->
                        <div class="lb-name">Rufit</div>
                        <div class="lb-job mb-20px">Operations Manager</div>
                        <p>We can show you his CV but his work speaks for itself. Rufit is like our very own Dr. House except that we actually like him. Rufit has worked with cars before he could walk and has plied his expertise on several continents in numerous countries. We didn’t even need to do a background check since the US Embassy of Kazakhstan did that for us when they hired him as their Chief Mechanic back in the ‘90s. His charm lies in his accent, so you won’t even mind not understanding him.</p>
                    </div><!--team-block-->
                </li>
                <li>
                    <div class="team-block">
                        <div class="team-image mb-20px">
                            <img src="<?= \yii\helpers\Url::to(['/images/s1.jpg'])?>" alt="" />
                        </div><!--team-image-->
                        <div class="lb-name">Ian</div>
                        <div class="lb-job mb-20px">Chief Electrician</div>
                        <p>Meet Ian, our Chief Electrician. He’s always positive, never negative and keeps the flow going (see what we did there). But don’t let that fool you. Given half the chance, he can modify your car into a hairdryer. Or something fun like a game console.</p>
                    </div><!--team-block-->
                </li>
                <li>
                    <div class="team-block">
                        <div class="team-image mb-20px">
                            <img src="<?= \yii\helpers\Url::to(['/images/s1.jpg'])?>" alt="" />
                        </div><!--team-image-->
                        <div class="lb-name">Dima</div>
                        <div class="lb-job mb-20px">Chief Mechanic</div>
                        <p>Armed with just a hammer and some screwdrivers, our Chief Mechanic Dima will make sure your car is fit and healthy, mechanically speaking. A few turns here, a couple of clicks there and you’re good to go. </p>
                    </div><!--team-block-->
                </li>
                <li>
                    <div class="team-block">
                        <div class="team-image mb-20px">
                            <img src="<?= \yii\helpers\Url::to(['/images/s1.jpg'])?>" alt="" />
                        </div><!--team-image-->
                        <div class="lb-name">Vina</div>
                        <div class="lb-job mb-20px">Office Administrator</div>
                        <p>It’s not easy being the only lady in our team of experts, but our admin queen Vina is more than up for the task and carries off the whole thing with grace and a huge smile.  </p>
                    </div><!--team-block-->
                </li>
            </ul>
        </div>
    </div><!--team-block-->
</div><!--about-us-->