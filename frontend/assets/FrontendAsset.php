<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class FrontendAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/style.css',
        'css/jquery.flipster.css',
        'css/transition.css',
        'css/owl.carousel.css',
        'css/owl.theme.css',
        'css/owl.transitions.css',
    ];
    public $js = [
        'js/scrollTo.js',
        'js/ticon.js',
        'js/jquery.touchSwipe.min.js',
        'js/scripts.js',
        'js/jquery.sticky.js',
        'js/jquery.flipster.js',
        'js/owl.carousel.js',
        'js/jquery.toaster.js'

    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'common\assets\FontAwesome',
        'common\assets\Html5shiv',
    ];
}
