<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use frontend\models\Lang;

/* @var $this yii\web\View */
/* @var $model common\models\Page */
/* @var $form yii\bootstrap\ActiveForm */
$lang_objs = Lang::find()->all();
$langs = array();
$flag_code = array();
$flag_code_options = array();
foreach ($lang_objs as $lo)
{
    $langs[$lo['local']] = $lo['name'];
    $flag_code[$lo['local']] = $lo['url'];
    $flag_code_options[$lo['local']] = [
        'data-flag' => $lo['url']
    ];
}
$flag_code[''] = Lang::getDefaultLang()['url'];
?>

<div class="page-form">

    <?php $form = ActiveForm::begin(); ?>

    <input type="hidden" value="<?=Yii::$app->request->getCsrfToken()?>" />

    <?=
    //$form->field($model, 'lang')->textInput(['maxlength' => 5])
    $form->field($model, 'lang', ['template' => '
<label for="page-lang" class="control-label">{label}</label>
<div style="clear: both"></div>
<div class="col-sm-1"><img id="lang-flag" src="/img/blank.gif" class="flag flag-'.$flag_code[$model->lang].'" alt="" /></div><div class="col-sm-11">{input}</div>

<p class="help-block help-block-error">{error}{hint}</p>
        '])->dropDownList($langs,["options" =>$flag_code_options])
    ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 512]) ?>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => 2048]) ?>

    <?= $form->field($model, 'slogan')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'vision')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'summary')->textarea([
        'rows' => 4
    ]) ?>

    <?= $form->field($model, 'body')->textarea([
    'rows' => 12
    ]) ?>

    <?= $form->field($model, 'status')->checkbox() ?>

    <?= \yii\bootstrap\Collapse::widget([
	    'items' => [
		    [
			    'label' => 'Meta information',
		        'content' =>
			        $form->field($model, 'meta_title')->textInput(['maxlength' => true])
			        . $form->field($model, 'meta_keywords')->textInput(['maxlength' => true])
			        . $form->field($model, 'meta_description')->textarea()
			    ,
		    ],
	    ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$this->registerJs(
    <<<JS
$( document ).ready(function() {
    $("#page-lang").change(
        function()
        {

            $("#lang-flag").removeClass();
            $("#lang-flag").addClass("flag flag-" + $("option:selected", this).data('flag'));
        }
    );
});
JS
);
?>