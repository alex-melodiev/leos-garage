<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use frontend\models\Lang;

/* @var $this yii\web\View */
/* @var $model common\models\Article */
/* @var $categories common\models\ArticleCategory[] */
/* @var $form yii\bootstrap\ActiveForm */

$lang_objs = Lang::find()->all();
$langs = array();
$flag_code = array();
$flag_code_options = array();
foreach ($lang_objs as $lo)
{
    $langs[$lo['local']] = $lo['name'];
    $flag_code[$lo['local']] = $lo['url'];
    $flag_code_options[$lo['local']] = [
        'data-flag' => $lo['url']
    ];
}

$flag_code[''] = Lang::getDefaultLang()['url'];
?>

<div class="article-form">

    <?php $form = ActiveForm::begin(); ?>

    <?=
    //$form->field($model, 'lang')->textInput(['maxlength' => 5])
    $form->field($model, 'lang', ['template' => '
<label for="page-lang" class="control-label">{label}</label>
<div style="clear: both"></div>
<div class="col-sm-1"><img id="lang-flag" src="/img/blank.gif" class="flag flag-'.$flag_code[$model->lang].'" alt="" /></div><div class="col-sm-11">{input}</div>

<p class="help-block help-block-error">{error}{hint}</p>
        '])->dropDownList($langs,["options" =>$flag_code_options])
    ?>


    <?= $form->field($model, 'title')->textInput(['maxlength' => 512]) ?>

    <?= $form->field($model, 'slug')
        ->hint(Yii::t('backend', 'If you\'ll leave this field empty, slug will be generated automatically'))
        ->textInput(['maxlength' => 1024]) ?>

    <?= $form->field($model, 'category_id')->dropDownList(\yii\helpers\ArrayHelper::map(
            $categories,
            'id',
            'title'
        ), ['prompt'=>'']) ?>

    <?= $form->field($model, 'body')->widget(
        \yii\imperavi\Widget::className(),
        [
            'plugins' => ['fullscreen', 'fontcolor', 'video'],
            'options'=>[
                'minHeight'=>400,
                'maxHeight'=>400,
                'buttonSource'=>true,
                'convertDivs'=>false,
                'removeEmptyTags'=>false,
                'imageUpload'=>Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi'])
            ]
        ]
    ) ?>

    <?= $form->field($model, 'attachments')->widget(
        \trntv\filekit\widget\Upload::className(),
        [
            'url'=>['/file-storage/upload'],
            'sortable'=>true,
            'maxFileSize'=>10000000, // 10 MiB
            'maxNumberOfFiles'=>20
        ]);
    ?>

    <?= $form->field($model, 'status')->checkbox() ?>

    <?= $form->field($model, 'published_at')->widget('trntv\yii\datetimepicker\DatetimepickerWidget') ?>

    <div class="form-group">
        <?= Html::submitButton(
            $model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
