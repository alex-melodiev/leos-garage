<?php

use yii\db\Schema;
use yii\db\Migration;

class m150820_160041_alter_page_add_keywords_description_columns extends Migration
{
    public function up()
    {
        $this->addColumn('page', 'meta_title', Schema::TYPE_TEXT . ' NOT NULL AFTER `body`');
        $this->addColumn('page', 'meta_keywords', Schema::TYPE_TEXT . ' NOT NULL AFTER `meta_title`');
        $this->addColumn('page', 'meta_description', Schema::TYPE_TEXT . ' NOT NULL AFTER `meta_keywords`');
    }

    public function down()
    {
        $this->dropColumn('page', 'meta_keywords');
        $this->dropColumn('page', 'meta_description');
    }
}
