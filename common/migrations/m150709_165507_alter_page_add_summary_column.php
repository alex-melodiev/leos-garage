<?php

use yii\db\Schema;
use yii\db\Migration;

class m150709_165507_alter_page_add_summary_column extends Migration
{
    public function up()
    {
	    $this->addColumn('page', 'summary', Schema::TYPE_TEXT . ' NOT NULL AFTER `vision`');
    }

    public function down()
    {
        $this->dropColumn('page', 'summary');
    }
    
}
