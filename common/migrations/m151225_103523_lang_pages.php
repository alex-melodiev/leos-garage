<?php

use yii\db\Schema;
use yii\db\Migration;

class m151225_103523_lang_pages extends Migration
{
    public function up()
    {

        $this->addColumn('page', 'lang', Schema::TYPE_STRING . '(5) NOT NULL AFTER `id`');


    }

    public function down()
    {
        $this->dropColumn('page', 'lang');

    }
    
    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
