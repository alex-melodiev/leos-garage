<?php

use yii\db\Schema;
use yii\db\Migration;

class m150708_180442_alter_page_add_slogan_vision_columns extends Migration
{
    public function up()
    {
	    $this->addColumn('page', 'slogan', Schema::TYPE_STRING . '(255) NOT NULL AFTER `title`');
	    $this->addColumn('page', 'vision', Schema::TYPE_STRING . '(100) NOT NULL AFTER `slogan`');
    }

    public function down()
    {
	    $this->dropColumn('page', 'slogan');
	    $this->dropColumn('page', 'vision');
    }
}
