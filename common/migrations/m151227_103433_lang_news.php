<?php

use yii\db\Schema;
use yii\db\Migration;

class m151227_103433_lang_news extends Migration
{
    public function up()
    {
        $this->addColumn('article', 'lang', Schema::TYPE_STRING . '(5) NOT NULL AFTER `id`');
    }

    public function down()
    {
        $this->dropColumn('article', 'lang');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }
    
    public function safeDown()
    {
    }
    */
}
