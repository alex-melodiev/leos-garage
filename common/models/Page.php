<?php

namespace common\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "page".
 *
 * @property integer $id
 * @property string $lang
 * @property string $slug
 * @property string $title
 * @property string $vision
 * @property string $slogan
 * @property string $summary
 * @property string $body
 * @property string $meta_title
 * @property string $meta_keywords
 * @property string $meta_description
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class Page extends \yii\db\ActiveRecord
{
    const STATUS_DRAFT = 0;
    const STATUS_PUBLISHED = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            'slug'=>[
                'class'=>SluggableBehavior::className(),
                'attribute'=>'title',
                'ensureUnique'=>true,
                'immutable'=>true
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lang','title', 'body'], 'required'],
            [['body','summary','slogan','vision', 'meta_keywords', 'meta_description'], 'string'],
            [['status'], 'integer'],
            [['slug'], 'string', 'max' => 2048],
            [['title'], 'string', 'max' => 512],
            [['lang'], 'string', 'max' => 5],
            [['vision', 'slogan', 'meta_title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'lang' => Yii::t('common', 'Language'),
            'slug' => Yii::t('common', 'Slug'),
            'title' => Yii::t('common', 'Title'),
            'slogan' => Yii::t('common', 'Slogan Subtitle'),
            'vision' => Yii::t('common', 'Vision Subtitle'),
            'summary' => Yii::t('common', 'Summary'),
            'body' => Yii::t('common', 'Body'),
            'status' => Yii::t('common', 'Active'),
        ];
    }
}
