<?php
/**
 * Created by PhpStorm.
 * User: dusty
 * Date: 23.12.15
 * Time: 13:29
 */
namespace common\components;

use yii\web\UrlManager;
use frontend\models\Lang;

class LangUrlManager extends UrlManager
{
    private $lang_urls;

    public function __construct($config = [])
    {
        parent::__construct($config);

        $urls_obj = Lang::find()->asArray()->all();

        $this->lang_urls = array();
        foreach($urls_obj as $u) $this->lang_urls[] = $u['url'];


    }

    public function createUrl($params)
    {

        if( isset($params['lang_id']) ){
            //Если указан идентификатор языка, то делаем попытку найти язык в БД,
            //иначе работаем с языком по умолчанию
            $lang = Lang::findOne($params['lang_id']);
            if( $lang === null ){
                $lang = Lang::getDefaultLang();
            }
            unset($params['lang_id']);
        } else {
            //Если не указан параметр языка, то работаем с текущим языком
            $lang = Lang::getCurrent();
        }

        //Получаем сформированный URL(без префикса идентификатора языка)
        $url = parent::createUrl($params);


        $pie = explode('/',$url);



        if (in_array($pie[1],$this->lang_urls))
        {
            unset($pie[1]);
            $url = implode('/',$pie);
        }

        //Добавляем к URL префикс - буквенный идентификатор языка


        if ($lang->id !== Lang::getDefaultLang()->id) {
            if ($url == '/') {
                return '/' . $lang->url;
            } else {
                return '/' . $lang->url . $url;
            }
        }
        else
        {
            return $url;
        }
    }
}
